var express = require('express');
var router = express.Router();
var apiai = require('apiai');
var app = apiai("b7cb07d4bd6d4092976812e90688fa77");

/* GET users listing. */
router.post('/', function(req, res, next) {
	var response = sendDataToApi(req.body.q, res);
	//res.json(response);
});

var sendDataToApi = (query, res) => {
	var request = app.textRequest(query, {
    sessionId: '7c014ea8-9316-4689-92e4-d5c7212efdab'
	});
	 
	request.on('response', function(response) {
	    res.json(response);
	});
	 
	request.on('error', function(error) {
	    res.json(error);
	});
	 
	request.end();
}

module.exports = router;
